# pogrep

Searches for string in po files. *popgrep* supports various *GNU grep* options
to ease its use.


## Pogrep is part of poutils!

[Poutils](https://pypi.org/project/poutils) (`.po` utils) is a metapackage to easily install useful Python tools to use with po files
and `pogrep` is a part of it! Go check out [Poutils](https://pypi.org/project/poutils) to discover the other tools!


## Examples

Print usage: `pogrep --help`

Find how 'flavors' has already been translated: search recursively in the
current directory, show the names of the matching files, excluding the venv and the
locales directories which are not relevant:

`pogrep --recursive --line-number --exclude-dir venv --exclude-dir locales flavor `

Search the word 'typo' in traductions, but not in sources:

`pogrep --recursive --translation --no-source --word-regexp typo `

Blame usage of 'allogène' in traductions:

`pogrep --recursive --ignore-case  --blame allogène `


### Searching in Debian manpages

To search for a translation in the Debian manpages translations:

```text
$ git clone https://salsa.debian.org/manpages-l10n-team/manpages-l10n
$ pogrep -r 'new pipe buffer' manpages-l10n-team/po/fr/
╒══════════════════════════════════════╤══════════════════════════════════════╕
│ Make the new pipe buffer allocation. │ Allocation du tampon pour le nouveau │
│                                      │ tube.                                │
├──────────────────────────────────────┼──────────────────────────────────────┤
│ Before Linux 4.9, bugs similar to    │ Avant Linux 4.9, des bogues          │
│ points (1) and (3) could also occur  │ similaires aux points (1) et (3)     │
│ when the kernel allocated memory for │ pourraient se produire quand le      │
│ a new pipe buffer; that is, when     │ noyau alloue de la mémoire pour le   │
│ calling B<pipe>(2)  and when opening │ nouveau tampon de tube, c’est-à-dire │
│ a previously unopened FIFO.          │ lors de l’appel B<pipe>(2) et lors   │
│                                      │ de l’ouverture d’une nouvelle FIFO   │
│                                      │ précédemment non ouverte.            │
╘══════════════════════════════════════╧══════════════════════════════════════╛
```

## Contributing

Please test your contribution using `tox -p auto`.
